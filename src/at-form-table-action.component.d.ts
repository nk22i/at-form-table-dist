import { AtFormTableComponent } from "./at-form-table.component";
import { EventEmitter } from '@angular/core';
export declare class AtFormTableActionComponent {
    action: AtAction;
    actionTriggered: EventEmitter<AtAction>;
    trigger(): void;
}
export declare class AtAction {
    alt?: string;
    icon: string;
    iconColor: string;
    class: string;
    action: any;
    table?: AtFormTableComponent;
    constructor(values?: any);
    static _action(act: AtAction, table: AtFormTableComponent, index: number): void;
}
