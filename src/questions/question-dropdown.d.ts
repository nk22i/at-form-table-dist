import { QuestionBase } from './question-base';
export declare class DropdownQuestion extends QuestionBase<string> {
    controlType: string;
    options: {
        keyValue: string;
        displayValue: string;
    }[];
    constructor(options?: {});
    static isDefault(defaultKey: string, key: string): string;
}
