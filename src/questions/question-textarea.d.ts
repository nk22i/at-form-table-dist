import { QuestionBase } from './question-base';
export declare class TextareaQuestion extends QuestionBase<string> {
    controlType: string;
    type: string;
    constructor(options?: {});
}
