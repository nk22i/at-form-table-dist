import { QuestionBase } from './question-base';
export declare class NumberQuestion extends QuestionBase<number> {
    controlType: string;
    type: string;
    constructor(options?: {});
}
