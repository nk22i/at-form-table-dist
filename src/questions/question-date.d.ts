import { QuestionBase } from './question-base';
export declare class DateQuestion extends QuestionBase<Date> {
    controlType: string;
    type: string;
    constructor(options?: {});
}
