import { ValidatorFn } from "@angular/forms/forms";
export declare class QuestionBase<T> {
    value: T;
    key: string;
    label: string;
    required: boolean;
    order: number;
    controlType: string;
    disabled: boolean;
    validators?: ValidatorFn[];
    customErrors?: any;
    width: string;
    constructor(options?: {
        value?: T;
        key?: string;
        label?: string;
        required?: boolean;
        order?: number;
        controlType?: string;
        disabled?: boolean;
        validators?: ValidatorFn[];
        customErrors?: string[];
        width?: string;
    });
    addValidator(validator: ValidatorFn): void;
    removeValidatorAt(index: number): ValidatorFn;
    getValidatorAt(index: number): ValidatorFn;
}
