import { QuestionBase } from './question-base';
export declare class EmailQuestion extends QuestionBase<string> {
    controlType: string;
    type: string;
    constructor(options?: {});
}
