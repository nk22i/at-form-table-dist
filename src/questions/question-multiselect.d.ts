import { QuestionBase } from './question-base';
export declare class MultiselectQuestion extends QuestionBase<string[]> {
    controlType: string;
    options: {
        key: string;
        value: string;
    }[];
    constructor(options?: {});
}
