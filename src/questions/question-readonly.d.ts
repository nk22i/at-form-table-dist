import { QuestionBase } from './question-base';
export declare class ReadonlyQuestion extends QuestionBase<string> {
    controlType: string;
    type: string;
    constructor(options?: {});
}
