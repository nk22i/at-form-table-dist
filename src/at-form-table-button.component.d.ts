import { AtFormTableComponent } from "./at-form-table.component";
export declare class AtFormTableButtonComponent {
    button: AtButton;
    tabindex: number;
}
export declare class AtButton {
    text: string;
    icon: string;
    iconColor: string;
    class: string;
    colspan?: number;
    align?: string;
    action: any;
    table?: AtFormTableComponent;
    constructor(values?: any);
    _action(): void;
}
