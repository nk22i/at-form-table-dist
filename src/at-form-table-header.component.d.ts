import { QuestionBase } from './questions/question-base';
export declare class AtFormTableHeaderComponent {
    headers: AtHeader[];
}
export interface AtHeader {
    title: string;
    subtitle?: string;
    field: string;
    question: QuestionBase<any>;
    placeholder: any;
}
